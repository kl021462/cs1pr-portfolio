/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);

void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap();
	
	stage.multiplier = 1; //initialise score multiplier
	stage.speedMultiplier = 1;

}

static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();
}

static void draw(void)
{
	SDL_SetRenderDrawColor(app.renderer, 128, 0, 255, 255);
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap();

	drawEntities();

	drawHud();
}

static void drawHud(void)
{
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	incremClock();
	if (stage.PU > 0)
	{
		checkPowerUp();
	}

	drawText(5, 5, 255, 255, 255, TEXT_LEFT, "LEVEL %d", game.level);
	drawText(SCREEN_WIDTH / 3 , 5, 255, 255, 255, TEXT_CENTER, "SCORE: %d", stage.score);

	if (stage.PU == 1) {
		drawText(SCREEN_WIDTH / 3, 45, 255, 165, 0, TEXT_CENTER, "DOUBLE POINTS!");
	}
	if (stage.PU == 2) {
		drawText(SCREEN_WIDTH / 3, 45, 255, 165, 0, TEXT_CENTER, "SUPER SPEED!");
	}
	
	if (stage.timeSec < 10) {
		drawText(SCREEN_WIDTH * 2 / 3, 5, 255, 255, 255, TEXT_CENTER, "TIME: %d.0%f", stage.timeMin, stage.timeSec);
	}
	else {
		drawText(SCREEN_WIDTH * 2 / 3, 5, 255, 255, 255, TEXT_CENTER, "TIME: %d.%f", stage.timeMin, stage.timeSec);
	}
	
	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "PIZZA %d/%d", stage.pizzaFound, stage.pizzaTotal);
}