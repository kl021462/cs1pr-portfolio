#include "common.h"

static void touchSpike(Entity* other);

void initSpike(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/spike.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->touch = touchSpike;

}

void initSpike5(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/spike5.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->touch = touchSpike;

}

static void touchSpike(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		player->health = 0;
		stage.score = stage.score - 100;
	}
}