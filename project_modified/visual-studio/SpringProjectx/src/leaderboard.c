#include "common.h"

static void logicLeaderboard(void);
static void drawLeaderboard(void);

void initLeaderboard(void)
{
	app.delegate.logic = logicLeaderboard;
	app.delegate.draw = drawLeaderboard;

	game.leaderboard = 1;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	game.map = "data/map_empty.dat";
	initMap();

}

static void logicLeaderboard(void)
{
	
	if (app.keyboard[SDL_SCANCODE_RETURN])
	{
		game.leaderboard = 0;
	}

	doCamera();
}


static void drawLeaderboard(void)
{
	SDL_SetRenderDrawColor(app.renderer, 128, 0, 255, 255);
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap();

	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(5, 5, 255, 255, 255, TEXT_LEFT, "FINAL SCORES AND TIMES");
	
	drawText(5, 45, 255, 255, 255, TEXT_LEFT, "LEVEL 1:");
	drawText(SCREEN_WIDTH / 3, 45, 255, 255, 255, TEXT_CENTER, "%d", game.level1.score);
	if (game.level1.timeSec < 10) {
		drawText(SCREEN_WIDTH * 2 / 3, 45, 255, 255, 255, TEXT_CENTER, "%d.0%f", game.level1.timeMin, game.level1.timeSec);
	}
	else {
		drawText(SCREEN_WIDTH * 2 / 3, 45, 255, 255, 255, TEXT_CENTER, "%d.%f", game.level1.timeMin, game.level1.timeSec);
	}
	
	drawText(5, 85, 255, 255, 255, TEXT_LEFT, "LEVEL 2:");
	drawText(SCREEN_WIDTH / 3, 85, 255, 255, 255, TEXT_CENTER, "%d", game.level2.score);
	if (game.level2.timeSec < 10) {
		drawText(SCREEN_WIDTH * 2 / 3, 85, 255, 255, 255, TEXT_CENTER, "%d.0%f", game.level2.timeMin, game.level2.timeSec);
	}
	else {
		drawText(SCREEN_WIDTH * 2 / 3, 85, 255, 255, 255, TEXT_CENTER, "%d.%f", game.level2.timeMin, game.level2.timeSec);
	}
	
	drawText(5, 125, 255, 255, 255, TEXT_LEFT, "LEVEL 3:");
	drawText(SCREEN_WIDTH / 3, 125, 255, 255, 255, TEXT_CENTER, "%d", game.level3.score);
	if (game.level3.timeSec < 10) {
		drawText(SCREEN_WIDTH * 2 / 3, 125, 255, 255, 255, TEXT_CENTER, "%d.0%f", game.level3.timeMin, game.level3.timeSec);
	}
	else {
		drawText(SCREEN_WIDTH * 2 / 3, 125, 255, 255, 255, TEXT_CENTER, "%d.%f", game.level3.timeMin, game.level3.timeSec);
	}
	
	game.total.score = game.level1.score + game.level2.score + game.level3.score;
	game.total.timeMin = game.level1.timeMin + game.level2.timeMin + game.level3.timeMin;
	game.total.timeSec = game.level1.timeSec + game.level2.timeSec + game.level3.timeSec;
	while (game.total.timeSec >= 60)
	{
		game.total.timeMin++;
		game.total.timeSec = game.total.timeSec - 60;
	}
	
	drawText(5, 165, 255, 255, 255, TEXT_LEFT, "TOTAL:");
	drawText(SCREEN_WIDTH / 3, 165, 255, 255, 255, TEXT_CENTER, "%d", game.total.score);
	if (game.total.timeSec < 10) {
		drawText(SCREEN_WIDTH * 2 / 3, 165, 255, 255, 255, TEXT_CENTER, "%d.0%f", game.total. timeMin, game.total.timeSec);
	}
	else {
		drawText(SCREEN_WIDTH * 2 / 3, 165, 255, 255, 255, TEXT_CENTER, "%d.%f", game.total.timeMin, game.total.timeSec);
	}
	drawText(5, 205, 255, 255, 255, TEXT_LEFT, "PRESS RETURN TO END GAME");

}

