#include "common.h"

static void tickDP(void);
static void touchDP(Entity* other);

void initDoublePoints(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/doublepoints.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tickDP;
	e->touch = touchDP;

}



static void tickDP(void)
{
	self->value += 0.1;

	self->y += sin(self->value);
}

static void touchDP(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		stage.PU = 1; //stage.PU == 1 means that double points is active
		stage.multiplier = 2;
		timePUStart = stage.timeSec;

	}
}