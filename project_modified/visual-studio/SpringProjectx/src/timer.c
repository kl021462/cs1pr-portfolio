#include "common.h"

void initClock(void);
void incremClock(void);
void bonusScore(void);

 void initClock(void) {
          
     timeStart = clock();
     stage.timeMin = 0;

 }

 void incremClock(void) {

     timeCurrent = clock() - timeStart;
     stage.timeSec = ((float)timeCurrent) / CLOCKS_PER_SEC;
     if (stage.timeSec >= 60) {
         stage.timeMin++;
         timeStart = clock();
     }

 }

 void bonusScore(void) {
     // if completed in under 3 mins award bomus points)
     if (stage.timeMin < 2 ) {
         stage.bonus = ( 120 - ( (stage.timeMin * 60) + (int)stage.timeSec) )^2;
         stage.score = stage.score + stage.bonus;
     }

 }

 // ends power-ups
 void checkPowerUp(void) {

	 if (timePUStart >= 30)
	 {
		 if ((stage.timeSec > (timePUStart - 30)) && (stage.timeSec < timePUStart))
		 {
			 stage.multiplier = 1;
			 stage.speedMultiplier = 1;
			 stage.PU = 0;

		 }
	 }

	 if (timePUStart < 30)
	 {
		 if (stage.timeSec > (timePUStart + 30))
		 {
			 stage.multiplier = 1;
			 stage.speedMultiplier = 1;
			 stage.PU = 0;

		 }
	 }
 }