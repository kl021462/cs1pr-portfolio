#include "common.h"

static void tickSS(void);
static void touchSS(Entity* other);

void initSuperSpeed(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/superspeed.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tickSS;
	e->touch = touchSS;

}



static void tickSS(void)
{
	self->value += 0.1;

	self->y += sin(self->value);
}

static void touchSS(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		stage.PU = 2; //stage.PU == 2 means that super speed is active
		stage.speedMultiplier = 2;
		timePUStart = stage.timeSec;

	}
}