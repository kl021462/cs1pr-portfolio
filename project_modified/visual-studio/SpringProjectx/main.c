/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void capFrameRate(long* then, float* remainder);

char* global_dir = "C:\\Users\\di918039\\source\\repos\\cs1pr-portfolio\\project-visual-studio\\SpringProject";


// MAIN W SWITCH
int WinMain() {
	long then;
	float remainder;

	memset(&app, 0, sizeof(App));
	app.textureTail = &app.textureHead;

	initSDL();

	atexit(cleanup);

	initGame();

	//initLevels();
	memset(&game, 0, sizeof(game));
	game.complete = 1;
	
	game.level = 1;

	//initClock();

	while (game.complete == 1)
	{
		switch (game.level) 
		{
		case 1:
			game.entiteis = "data/ents01.dat";
			game.map = "data/map01.dat";
			break;
		
		case 2:
			game.level1.score = stage.score;
			game.level1.timeMin = stage.timeMin;
			game.level1.timeSec = stage.timeSec;

			game.entiteis = "data/ents02.dat";
			game.map = "data/map02.dat";
			break;
			
		case 3:
			game.level2.score = stage.score;
			game.level2.timeMin = stage.timeMin;
			game.level2.timeSec = stage.timeSec;
		

			game.entiteis = "data/ents03.dat";
			game.map = "data/map03.dat";
			break;
		}

		initClock();
		
		initStage();

		then = SDL_GetTicks();

		remainder = 0;

		stage.complete = 1;

		while (stage.complete == 1)
		{
			prepareScene();

			doInput();

			app.delegate.logic();

			app.delegate.draw();

			presentScene();

			capFrameRate(&then, &remainder);

		}
		
		bonusScore(); // adds bonus score
		game.level++; // end of level admin

		if (game.level > 3) // game end
		{
			game.level3.score = stage.score;
			game.level3.timeMin = stage.timeMin;
			game.level3.timeSec = stage.timeSec;
		
			game.complete = 0;
		}
		
	}

	initLeaderboard();

	while (game.leaderboard == 1)
	{
		prepareScene();

		doInput();

		app.delegate.logic();

		app.delegate.draw();

		presentScene();

		capFrameRate(&then, &remainder);
	}

return 0;

}

static void capFrameRate(long* then, float* remainder){
	long wait, frameTime;

	wait = 16 + *remainder;

	*remainder -= (int)* remainder;

	frameTime = SDL_GetTicks() - *then;

	wait -= frameTime;

	if (wait < 1)
	{
		wait = 1;
	}

	SDL_Delay(wait);

	*remainder += 0.667;

	*then = SDL_GetTicks();
}
